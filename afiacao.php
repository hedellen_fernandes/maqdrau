<?php include "header.php";?>
	<section id="corpoServico" class="afiacao">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h1 class="sub">AFIAÇÃO DE FERRAMENTAS</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<img class="img-responsive" src="assets/img/servicos/afiacao.jpg" height="309" width="542" alt="Carro Assistência Técnica Maqdrau">
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<p>Oferecemos a nossos clientes os serviços de afiação para punções, matrizes e facas, tanto aplicados ao sistema modular quanto para puncionadeiras. 
						Dispomos de centro móvel de afiação que consiste em um veículo equipado com retífica plana, desmagnetizador de ferramentas e peças de reposição para o sistema modular.
					</p>
					<p>Nossa unidade móvel realiza os serviços de afiação nas dependências de sua empresa, minimizando paradas de produção por deficiência de ferramental. Oferecemos flexibilidade de agenda, adequando nossa rotina as necessidades de nossos clientes.</p>
				</div>
			</div>
		</div>
	</section>
<?php include "footer.php";?>
