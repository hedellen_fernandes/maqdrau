var SPMaskBehavior = function (val) {
  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
spOptions = {
  onKeyPress: function(val, e, field, options) {
      field.mask(SPMaskBehavior.apply({}, arguments), options);
    }
};

$('.sp_celphones').mask(SPMaskBehavior, spOptions);


$(document).ready( function() {
    $("#contactForm").validate({
        // Define as regras
        rules: {
            nome: {
                // campoNome será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true
            },
            email: {
                // campoEmail será obrigatório (required) e precisará ser um e-mail válido (email)
                required: true
            },
            mensagem: {
                // campoMensagem será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true
            },
            telefone: {
                // campoMensagem será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true, minlength: 10
            },
        },
        // Define as mensagens de erro para cada regra
        messages: {
            nome: {
                required: "Digite Seu Nome."
            },
            email: {
                required: "Digite Seu E-mail.", email: "Digite um e-mail válido"
            },
            mensagem: {
                required: "Digite Sua Mensagem."
            },
            telefone: {
                required: "Digite Seu Telefone.", minLength: "Telefone Inválido"
            }            
        }
    });
});

var elem = document.getElementById('listaMaq');
var tipo = elem.getAttribute('data-maq');

