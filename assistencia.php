<?php include("header.php");?>
	<section id="corpoServico" class="assistencia">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h1 class="sub">ASSISTÊNCIA TÉCNICA</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-sm-8 col-xs-12">
					<img class="img-responsive" src="assets/img/servicos/assistencia.jpg" alt="Carro Assistência Técnica Maqdrau">
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<p>Oferecemos aos nossos clientes total suporte técnico. 
					Dispomos de equipe técnica altamente treinada, estoque local de peças para reposição,
					veículos novos e ferramental completo de forma a prestar atendimento rápido e eficaz, 
					minimizando tempo de máquina parada e interrupções de produção. 
					Além das rotinas corretivas, também oferecemos manutenção preventiva, como parte do nosso programa de pós venda.
					Nossa equipe técnica está habilitada para manutenção, podendo assim oferecer total respaldo a nossos cliente,
					garantindo assim que o atendimento aconteça em um curto espaço de tempo e atuando de forma efetiva quer seja para a entrega técnica, treinamento operacional, manutenção corretiva ou preventiva.</p>
				</div>
			</div>
		</div>
	</section>
<?php include "footer.php";?>