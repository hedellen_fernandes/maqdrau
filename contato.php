<?php include "header.php";?>
	<section id="topo" class="contato">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-sx-12">
					<h1>ENTRE EM CONTATO CONOSCO</h1>
					<h2>PREENCHA OS CAMPOS ABAIXO E AGUARDE O CONTATO DE REPRESENTANTE.</h2>
				</div>
			</div>
		</div>
	</section>
	<section id="corpo" class="contato">
		<div class="container">
			<form action="mail.php" method="POST" id="contactForm">
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<input type="text" name="nome" placeholder="NOME/EMPRESA">
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<input type="email" name="email" placeholder="E-MAIL">
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<input type="tel" name="telefone" placeholder="TELEFONE" class="sp_celphones">
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<span class="select">
							<select name="motivo">
								<option disabled selected>SELECIONE AQUI O MOTIVO DO CONTATO</option>
								<option value="Suporte Técnico">Suporte Técnico</option>
								<option value="Orçamento">Orçamento</option>
								<option value="Outros">Outros</option>
							</select>
						</span>
					</div>
				</div>
				<!-- <div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<span class="select">
							<select name="motivo">
								<option disabled selected>SELECIONE AQUI O TIPO DA MAQUINA</option>
								<option value="Suporte Técnico">Suporte Técnico</option>
								<option value="Orçamento">Orçamento</option>
								<option value="Outros">Outros</option>
							</select>
						</span>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<span class="select">
							<select name="motivo">
								<option disabled selected>SELECIONE AQUI O MODELO DA MAQUINA</option>
								<option value="Suporte Técnico">Suporte Técnico</option>
								<option value="Orçamento">Orçamento</option>
								<option value="Outros">Outros</option>
							</select>
						</span>
					</div>
				</div> -->
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<textarea name="mensagem" placeholder="DIGITE AQUI SUA MENSAGEM"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5 col-sm-12 col-xs 12">
						<p class="error">
					        <?php if(isset($_GET['msg'])):?>
			  					Sua mensagem foi enviada com sucesso!
					        <?php endif?>
					        <?php if(isset($_GET['err'])):?>
			  					Ops! Houve um erro, tente novamente!
					        <?php endif?>
						</p>
					</div>
					<div class="col-md-offset-5 col-md-2 col-sm-offset-2 col-sm-10 col-xs-12">
						<span class="submit">
							<input type="submit" class="btn btn-danger" value="ENVIAR">
						</span>
					</div>
				</div>
			</form>
		</div>
	</section>
<?php include "footer.php";?>