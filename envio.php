<?php
// Inclui o arquivo class.phpmailer.php localizado na pasta phpmailer
include "assets/component/phpmailer/class.smtp.php";
require_once("assets/component/phpmailer/class.phpmailer.php");

$nome = $_POST['nome'];
$email = $_POST['email'];
$telefone = $_POST['telefone'];
$motivo = $_POST['motivo'];
$mensagem = $_POST['mensagem'];

// Inicia a classe PHPMailer
$mail = new PHPMailer();
// Define os dados do servidor e tipo de conexão
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->IsSMTP(); // Define que a mensagem será SMTP
$mail->Host = "mail.maqdrau.com.br"; // Endereço do servidor SMTP
$mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
$mail->Username = 'form@maqdrau.com.br'; // Usuário do servidor SMTP
$mail->Password = 'maqdrau2016'; // Senha do servidor SMTP
// Define o remetente
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->From = "form@maqdrau.com.br"; // Seu e-mail
$mail->FromName = "Formulário de Contato Site"; // Seu nome
// Define os destinatário(s)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->AddAddress('contato@maqdrau.com.br', 'Contato Maqdrau');
$mail->AddAddress('contato@maqdrau.com.br');
//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta
// Define os dados técnicos da Mensagem
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
$mail->CharSet = 'UTF-8'; // Charset da mensagem (opcional)
// Define a mensagem (Texto e Assunto)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->Subject  = "Nova Solicitação de Contato"; // Assunto da mensagem
$mail->Body = "Nome: $nome <br>
			   E-mail: $email <br>
			   Telefone: $telefone <br>
			   Motivo de Contato: $motivo <br>
			   Mensagem: $mensagem";
$mail->AltBody = "Este é o corpo da mensagem de teste, em Texto Plano! \r\n :)";
// Define os anexos (opcional)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//$mail->AddAttachment("c:/temp/documento.pdf", "novo_nome.pdf");  // Insere um anexo
// Envia o e-mail
$enviado = $mail->Send();
// Limpa os destinatários e os anexos
$mail->ClearAllRecipients();
$mail->ClearAttachments();
// Exibe uma mensagem de resultado
if ($enviado) {
	header('location:contato.php?msg="Enviado com sucesso"');
} else {
	header('location:contato.php?err="Houve erro"');
}