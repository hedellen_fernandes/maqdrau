		<footer class="fundo-padrao">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<ul class="contato-info">
							<li>
								<img class="icons-yellow" src="assets/img/footer/icone-horariodeatendimento.svg" alt="Atendimento segunda à sexta / 7h30 às 17h30">
								Atendimento <span>segunda à sexta</span> / <span>7h30</span> às <span>17h30</span>
							</li>
							<li>
								<img class="icons-yellow" src="assets/img/footer/icone-email.svg" alt="contato@maqdrau.com.br">
								E-mail
								<span>contato@maqdrau.com.br</span>
							</li>
							<li>
								<img class="icons-yellow" src="assets/img/footer/icone-telefone.svg" alt="Telefones Para Contato">
								11 <span>4812-4981</span>/ 11 <span>9 6632-7260</span>/ 11 <span>9 5577-2484</span>
							</li>
						</ul>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<ul>
							<li><img class="icons-yellow" src="assets/img/footer/icone-linhasdecredito.svg" alt="LINHAS DE CRÉDITO">LINHAS DE <span>CRÉDITO</span></li>
							<li>
								<img src="assets/img/footer/bndes.png" alt="BNDS">
								<img src="assets/img/footer/finame.png" alt="FINAME">
								<img src="assets/img/footer/leasing.png" alt="LEASING">
								<img src="assets/img/footer/proger.png" alt="PROGER">
								<img src="assets/img/footer/cartao-bndes.png" alt="CARTÃO BNDS">							
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="total">
			<div class="container sub-footer">
					<div class="row">
						<div class="col-md-1 col-sm-12 col-xs-12">
							<img src="assets/img/logo_maqdrau.svg" alt="MAQDRAU">
						</div>
						<div class="col-md-11 col-sm-12 col-xs-12">
							<p>Reservamos o direito de modificar os modelos de nossos produtos sem aviso prévio. <br> <span>Copyright &copy; MAQDRAU - 2016 - Todos os direitos são reservados.</span></p>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</body>
</html>