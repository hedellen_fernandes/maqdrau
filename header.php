<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>
		<?php
			$root = $_SERVER['DOCUMENT_ROOT'];
			$page = basename($_SERVER['SCRIPT_NAME']);
			function title_pagina(){
				$page = basename($_SERVER['SCRIPT_NAME']);
				if($page == "index.php") {echo "Início";}//INICIO
				if($page == "servicos.php") {echo "Serviços";}//SERVICOS
				if($page == "maquinas.php") {echo "Máquinas";}//MAQUINAS
				if($page == "contato.php") {echo "Contato/Orçamento";}//CONTATO/ORCAMENTO
				if($page == "treinamento.php") {echo "Treinamento";}//TREINAMENTO
				if($page == "afiacao.php") {echo "Afiação";}//AFIACAO
				if($page == "adquacao.php") {echo "Adequação";}//ADQUACAO
				if($page == "assistencia.php") {echo "Assistência Técnica";}//ADQUACAO
			}
			echo title_pagina(). " | Maqdrau";
		?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="assets/component/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<script type="text/javascript" src="assets/component/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="assets/component/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/component/mask-plugin/dist/jquery.mask.min.js"></script>
	<script type="text/javascript" src="assets/component/jquery-validation/jquery.validate.js"></script>
	<script type="text/javascript" src="assets/js/function.js"></script>

	<meta http-equiv= "Content-Language" content= "pt-br">

	<meta property="og:title" content="Maqdrau">
	<meta property="og:description" content="MAqdrau é uma empresa que oferece serviços e vendas de máquinas.">
	<meta property="og:type" content="website">
	<meta property="og:site_name" content="Maqdrau">
	<meta property="og:url" content="http://maqdrau.com.br">
	<meta property="og:image" content="assets/img/icons/face-twitter.png">

	<meta name="twitter:card" content="MAqdrau é uma empresa que oferece serviços e vendas de máquinas.">
	<meta name="twitter:site" content="http://maqdrau.com.br">
	<meta name="twitter:creator" content="MAQDRAU">
	<meta name="twitter:title" content="Maqdrau">
	<meta name="twitter:description" content="MAqdrau é uma empresa que oferece serviços e vendas de máquinas.">
	<meta name="twitter:image" content="assets/img/icons/face-twitter.png">

	<link rel="apple-touch-icon" sizes="57x57" href="assets/img/icons/apple/57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="assets/img/icons/apple/60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/img/icons/apple/72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/icons/apple/76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/img/icons/apple/114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="assets/img/icons/apple/120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="assets/img/icons/apple/144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="assets/img/icons/apple/152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="assets/img/icons/apple/180x180.png">

	<link rel="icon" type="image/png" href="assets/img/icons/android/32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="assets/img/icons/android/192x192.png" sizes="192x192">

	<link rel="icon" type="image/png" href="assets/img/icons/favicon114x114.png" sizes="96x96">
	<link rel="icon" type="image/png" href="assets/img/icons/favicon16x16.ico" sizes="16x16">		
	<link rel="shortcut icon" href="assets/img/icons/favicon16x16.ico">
	<meta name="msapplication-TileColor" content="#18bd5e">
	<meta name="msapplication-TileImage" content="assets/img/icons/windows10.png">
	<meta name="theme-color" content="#00052d">

	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="Maqdrau">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navPrincipal" aria-expanded="false">
					<span class="sr-only">Botão de Menu</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<p>MENU</p>
				</button>
				<a class="navbar-brand" href="index.php" title="Logo MAQDRAU">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="145.222" height="61.883" viewBox="0 0 109.222 47.883">
						<defs>
							<path id="a" d="M0 0h109.222v47.883H0z"/>
						</defs>
						<clipPath id="b"><use xlink:href="#a" overflow="visible"/></clipPath>
						<path clip-path="url(#b)" fill="#FFF" d="M77.187 6.584l-5.132
						16.604h-5.942L74.575.123h5.223l8.527 23.065H82.35l-.785-2.513-4.378-14.09zm26.38 4.804c0-2.303-.384-4.056-1.147-5.25-.767-1.196-1.86-1.79-3.287-1.79-1.47 0-2.574.59-3.31 1.768-.74 1.178-1.113 2.903-1.124 5.18v1.065c0 2.283.372 4.034 1.123 5.25.747 1.217 1.86 1.823 3.342 1.823 1.417 0 2.498-.6 3.255-1.8.75-1.2 1.135-2.926 1.147-5.175v-1.07zm5.655.946c0 2.135-.35 3.996-1.06 5.585-.707 1.594-1.687 2.87-2.93 3.827l3.72 2.952-3.433 2.914-4.96-3.996c-.465.065-.93.097-1.395.097-1.962 0-3.71-.454-5.228-1.362-1.52-.91-2.71-2.212-3.558-3.9-.85-1.69-1.287-3.637-1.32-5.833v-1.222c0-2.27.41-4.27 1.243-5.99.827-1.726 2.01-3.056 3.547-3.996C95.386.472 97.15 0 99.133 0c1.952 0 3.693.466 5.228 1.395 1.53.93 2.72 2.255 3.57 3.97.854 1.718 1.282 3.686 1.292 5.91v1.06zM46.485.324l5.148 16.05 5.13-16.05h7.322v23.067H58.51v-5.384l.536-11.025-5.575 16.41h-3.675l-5.59-16.425.54 11.04v5.386H39.18V.326h7.305z"/>
						<path clip-path="url(#b)" fill="#FEC20F" d="M30.214.197h3.743V4.85l-1.926-.794c-.73-.236-1.417-1.044-1.522-1.805L30.214.198zm3.743 34.844v12.843H17.014c-.78.005-1.56-.155-2.14-.48l-12.77-7.277C.962 39.486 0 37.838 0 36.51V11.57c0-1.328.96-2.976 2.105-3.616L14.873.677c.58-.325 1.363-.486 2.14-.48h6.904l-.294 2.054c-.11.76-.794 1.57-1.524 1.806L19.35 5.19c-.678.353-1.734.27-2.346-.19l-2.45-1.837c-.612-.46-1.584-.424-2.156.083l-2.014 2.02c-.506.573-.548 1.546-.087 2.16l1.836 2.447c.462.613.543 1.67.187 2.35l-1.133 2.754c-.232.73-1.042 1.415-1.805 1.522l-3.025.43c-.758.11-1.442.822-1.523 1.584 0 0-.05.475-.05 1.433 0 .96.05 1.43.05 1.43.08.766.765 1.474 1.523 1.584l3.025.43c.76.113 1.573.796 1.805 1.526l1.133 2.752c.356.678.273 1.738-.187 2.35l-1.836 2.447c-.46.615-.57 1.45-.245 1.848.325.404 1.257 1.397 1.26 1.402 0 .004.316.29.697.64.376.347 1.932.833 2.545.376l2.447-1.84c.612-.457 1.667-.544 2.348-.185l2.748 1.13c.73.234 1.416 1.044 1.522 1.806l.433 3.03c.107.757.818 1.447 1.583 1.527 0 0 .472.05 1.428.05.955 0 1.43-.05 1.43-.05.758-.08 1.473-.77 1.58-1.528l.434-3.03c.105-.757.79-1.572 1.52-1.804l1.93-.795zM28.55 7.23c1.974.228 3.812.907 5.407 1.93V13.958c-1.368-1.572-3.263-2.674-5.407-3.025V7.23zM17.203 28.103c-1.832-2.215-2.932-5.057-2.932-8.156 0-6.57 4.942-11.98 11.31-12.717v3.702c-4.334.712-7.652 4.483-7.652 9.015 0 2.085.703 4.01 1.884 5.55l-2.61 2.606zm16.754 2.412v.22c-1.99 1.275-4.355 2.015-6.892 2.015-2.9 0-5.572-.965-7.717-2.59l2.627-2.63c1.456.98 3.207 1.553 5.09 1.553 1.575 0 3.06-.4 4.354-1.106l2.537 2.538zm0-15.557v8.73l-3.696-3.696c-2.203-2.204-5.616 1.21-3.412 3.414l4.11 4.11c-1.168.602-2.49.943-3.893.943-1.736 0-3.352-.524-4.7-1.42.63-1.215-.877-2.68-2.08-1.952-1.087-1.43-1.733-3.212-1.733-5.14 0-4.208 3.07-7.712 7.085-8.393.374 1.338 2.482 1.337 2.856 0 2.238.38 4.182 1.636 5.464 3.403"/><path clip-path="url(#b)" fill="#FFF" d="M109.222 28.057v12.8c0 1.453-.307 2.71-.925 3.768-.62 1.062-1.503 1.868-2.66 2.423-1.154.555-2.52.835-4.092.835-2.382 0-4.256-.62-5.628-1.852-1.37-1.236-2.066-2.928-2.094-5.077V28.057h4.74v12.988c.054 2.14 1.05 3.208 2.982 3.208.972 0 1.715-.27 2.214-.807s.75-1.41.75-2.616V28.057h4.712zM68.635 39.523c1.175-.555 2.036-1.278 2.585-2.172.55-.898.826-2.025.826-3.386 0-1.88-.652-3.332-1.958-4.363-1.31-1.03-3.12-1.543-5.44-1.543h-7.694v19.55h4.715v-6.9h2.547l3.452 6.9h5.05v-.2l-4.085-7.884zM66.67 36.37c-.454.472-1.128.706-2.022.706h-2.98v-5.39h2.98c.885 0 1.56.238 2.012.72.46.48.688 1.146.688 1.99 0 .842-.225 1.503-.678 1.974M53.365 32.548c-.77-1.43-1.847-2.535-3.226-3.318-1.38-.784-2.934-1.173-4.663-1.173H39.18v19.55h6.435c1.692-.008 3.223-.41 4.597-1.213 1.375-.803 2.438-1.912 3.195-3.333.755-1.415 1.136-3.024 1.136-4.81v-.9c-.01-1.772-.403-3.372-1.178-4.802m-3.63 5.692c0 1.902-.36 3.332-1.086 4.295-.725.96-1.765 1.443-3.117 1.443h-1.636V31.686h1.58c1.39 0 2.448.482 3.172 1.44.724.957 1.086 2.378 1.086 4.27v.844zM83.74 33.533l-4.348 
						14.075h-5.037l7.173-19.55h4.427l7.228 19.55h-5.065l-.664-2.13"/>
					</svg>
				</a>
			</div>
			<div class="collapse navbar-collapse" id="navPrincipal">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="/index.php" title="MAQDRAU" <?php if ($page == 'index.php'){echo 'class="active"';}?>>INÍCIO <span></span></a></li>
					<li><a href="/adquacao-nr-12" title="SERVIÇOS" <?php if ($page == "treinamento.php" || $page == "afiacao.php" || $page == "adquacao.php" || $page == "assistencia.php"){echo 'class="active"';}?>>SERVIÇOS</a></li>
					<li><a href="/maquinas-principal" title="MÁQUINAS" <?php if ($page == 'maquinas.php'){echo 'class="active"';}?>>MÁQUINAS</a></li>
					<li><a href="/contato" title="CONTATO/ORÇAMENTO" class="no-padding <?php if ($page == 'contato.php'){echo 'active"';}?>">CONTATO/ORÇAMENTO</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<?php if($page == "treinamento.php" || $page == "afiacao.php" || $page == "adquacao.php" || $page == "assistencia.php"):?>
		<nav class="navbar navbar-default navbar-servicos">
			<div class="container">
				<div class="navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="/adquacao-nr-12" title="ADQUAÇÃO NR12" <?php if ($page == 'adquacao.php'){echo 'class="active"';}?>>ADEQUAÇÃO NR12<span></span></a></li>
						<li><a href="/assistencia-tecnica" title="ASSISTÊNCIA TÉCNICA" <?php if ($page == 'assistencia.php'){echo 'class="active"';}?>>ASSISTÊNCIA TÉCNICA</a></li>
						<li><a href="/afiacao-ferramentas" title="AFIAÇÃO DE FERRAMENTAS" <?php if ($page == 'afiacao.php'){echo 'class="active"';}?>>AFIAÇÃO DE FERRAMENTAS</a></li>
						<li><a href="/treinamento-operacional" title="TREINAMENTO OPERACIONAL" <?php if ($page == 'treinamento.php'){echo 'class="active"';}?>>TREINAMENTO OPERACIONAL</a></li>
					</ul>
				</div>
			</div>
		</nav> 
	<?php endif;?>







