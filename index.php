<?php include "header.php";?>
	<!-- //TOPO -->
	<section id="slide">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="2700">
		<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<a href="catalogo.pdf" target="_blank" title="Baixe Nosso Catalogo">
						<img src="assets/img/slider/slider1.jpg" alt="Baixe Nosso Catalogo">
					</a>
				</div>
				<div class="item">
					<a href="maquinas/curvadoras/subc.php" title="Veja Nossas Curvadoras">
						<img src="assets/img/slider/curvadoras.jpg" alt="Veja Nossas Curvadoras">
					</a>
				</div>
				<div class="item">
					<a href="maquinas/dobradeiras/dobradeiras.php" title="Veja Nossas Dobradeiras">
						<img src="assets/img/slider/dobradeiras.jpg" alt="Veja Nossas Dobradeiras">
					</a>
				</div>
				<div class="item">
					<a href="maquinas/guilhotinas/guilhotina.php" title="Veja Nossas Guilhotinas">
						<img src="assets/img/slider/guilhotinas.jpg" alt="Veja Nossas Guilhotinas">
					</a>
				</div>
				<div class="item">
					<a href="maquinas/perfiladeiras/subp.php" title="Veja Nossas Perfiladeiras">
						<img src="assets/img/slider/perfiladeiras.jpg" alt="Veja Nossas Perfiladeiras">
					</a>
				</div>
				<div class="item">
					<a href="maquinas/plasmas/subpl.php" title="Veja Nossas Plasmas">
						<img src="assets/img/slider/plasmas.jpg" alt="Veja Nossas Plasmas">
				</a>
				</div>
				<div class="item">
					<a href="maquinas/prensas/subprs.php" title="Veja Nossas Prensas">
						<img src="assets/img/slider/prensas.jpg" alt="Veja Nossas Prensas">
					</a>
				</div>
		<!-- Controls -->
			</div>
		</div>
	</section>
	<section id="responsivo">
	</section>
	<section id="downCat">
		<div class="container">
			<div class="row"><a href="catalogo.pdf" class="btn bnt-alert" title="VEJA NOSSO CATALOGO 2016" target="_blank">VEJA NOSSO CATALOGO <span>2016</span></a></div>
		</div>
	</section>
	<section id="controle">
		<div class="container">
			<div class="row">
				<div class="container-control">
					<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"></a>
					<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"></a>
				</div>
			</div>
		</div>
	</section>
	<section id="opcoes" class="fundo-padrao">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12">
					<a href="/maquinas-principal" title="Máquinas" class="maquina">
						<h3>MÁQUINAS<img src="assets/img/icons/maquinas.svg" alt="Máquinas"></h3>
						<p>CONHEÇA A LINHA DE PRODUTOS QUE ATUAMOS, FERRAMENTAS, MÁQUINAS DE PRENSAS E HIDRÁULICAS.</p>
					</a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<a href="/adquacao-nr-12" title="Serviços" class="servico">
						<h3>SERVIÇOS <img src="assets/img/icons/servicos.svg" alt="Serviços"></h3>
						<p>AFIAÇÃO DE FERRAMENTAS, ASSISTÊNCIA TÉCNICA, ENTREGA TÉCNICA E TREINAMENTO OPERACIONAL.</p>
					</a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<a href="/contato" title="Orçamento" class="orcamento">
						<h3>ORÇAMENTO<img src="assets/img/icons/orcamentos.svg" alt="Orçamentos"></h3>
						<p>O SEU ORÇAMENTO É MUITO BEM-VINDO E RECEBERÁ ATENÇÃO ESPECIAL DOS NOSSOS CONSULTORES TÉCNICOS.</p>
					</a>
				</div>
			</div>
		</div>
	</section>
<?php include "footer.php";?>