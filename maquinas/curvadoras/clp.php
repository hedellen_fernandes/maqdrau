<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subc.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="twsub">Máquinas de Curvar Tubos a frio Automática - CLP</h1>
					<h2 class="lalign">MODELO CTA-2002 CLP</h2>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<img src="../../assets/img/maquinas/curvadora/clp.jpg" alt="Máquinas de Curvar Tubos a frio Automática- CLP" class="img-responsive">
			</div>
			<div class="row">
				<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
				<p>
					Máquina hidráulica motorizada para curvar tubos com parede fina ou grossa de marca Farex, modelo CTA-2002 CLP, com as seguintes características: <br>
					Estrutura reforçada construída com chapas de aço laminado ASTM A-36, soldadas entre si e submetidas a alivio de tensões, com fixação e alojamento na sua parte interna do mecanismo hidráulico de giro, atuadores hidráulicos, transmissão e correntes e a unidade hidráulica de acionamento. <br>
					Com a utilização do mandril interno ou espiga, a deformação externa na zona da curva fica reduzida ao mínimo. <br>
					Sistema hidráulico para posicionamento da régua, avanço e retorno da ferramenta da régua, avanço e retorno da morsa hidráulica (ou mordente) e giro do braço. <br>
					Painel de comando do tipo CLP (Controle Lógico Programável), montado em pedestal próprio móvel sobre rodas e separado da máquina. <br>
					Diâmetros a curvar de Ø 32 mm até Ø 219 mm.
				</p>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>
