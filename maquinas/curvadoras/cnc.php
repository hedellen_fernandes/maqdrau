<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subc.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="twsub">Máquinas de Curvar Tubos a frio Automática – CNC</h1>
					<h2 class="lalign">MODELO CTA-2002 CNC COM 03 EIXOS PRINCIPAIS "X","Y" E "Z"</h2>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<img src="../../assets/img/maquinas/curvadora/cnc.jpg" alt="Máquinas de Curvar Tubos a frio Automática – CNC" class="img-responsive">
			</div>
			<div class="row">
				<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
				<p>
					Máquina hidráulica motorizada para curvar tubos com parede fina ou grossa de marca FAREX, modelo CTA-2002 CNC com 03 eixos principais “X”, ”Y” e “Z”, abaixo descritos e monitorados eletronicamente e demais eixos de acionamentos hidráulicos e elétricos. <br>
					Estrutura reforçada construída com chapas de aço laminado ASTM A-36, soldadas entre si, com superfície da mesa usinada, com guias lineares e mancais de esferas, alojamento na parte inferior para o mecanismo de giro (Hidráulico), unidade hidráulica de acionamento com válvulas proporcionais de pressão e vazão e blocos hidráulicos, painel elétrico posicionado na parte traseira da mesma. <br>
					A Curvadora utiliza o processo de eixo fixo central, com mandril interno e braço de curvatura móvel, com o tubo sendo arrastado pelo giro do braço entorno da matriz. <br>
					Possibilidade de uso com mandril interno ou espiga e opcionalmente de tira rugas. <br>
					Pedestal de comando separado do corpo da estrutura. <br>
					Comando por CNC que possibilita desenhar ou importar os desenhos das peças segundo alguns critérios.
					Diâmetros a curvar de Ø 32mm até Ø 219mm.
				</p>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>
