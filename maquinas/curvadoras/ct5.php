<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subc.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="vcenter">Máquina de Curvar tubos elétrica – CT 5</h1>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<img src="../../assets/img/maquinas/curvadora/ct5.jpg" alt="Máquina de Curvar tubos elétrica – CT 5" class="img-responsive">
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
					<p class="desc">
						Máquina para curvar tubos motorizada, marca Farex, modelo CT-5, com coluna de perfil redondo, cabeçote superior em Ferro Nodular, base da coluna em chapa de aço laminado, com 7 matrizes e 7 réguas usinadas, para tubos de Ø 3/8” a 1.1/4” com paredes de até 1,5 mm de espessura. <br>
						O sistema de acionamento de giro é efetuado por meio de um sistema de motoredutor com freio eletromagnético, posição final atingida pelo sinal de comando de um sensor indutivo, com posição regulável.
					</p>
				</div>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>