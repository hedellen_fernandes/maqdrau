<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subc.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="twsub">Máquina de curvar tubos hidráulica – CT 6</h1>
					<h2 class="lalign">ACIONAMENTO MOTORIZADO - MODELO CT-6</h2>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<img src="../../assets/img/maquinas/curvadora/ct6.jpg" alt="Máquina de curvar tubos hidráulica – acionamento motorizado – CT 6" class="img-responsive">
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
					<p class="desc">
						Máquina hidráulica motorizada de curvar tubos marca Farex, modelo CT-6, para tubos de Ø ½ ” a 6 ” (Norma DIN 2440/2441 ou Schedulle 40 / 80), com 11 estampos, 2 contra estampos, unidade motriz de acionamento com válvula direcional manual, motor elétrico trifásico, mangueiras de interligação e conexões de engate rápido.
					</p>
				</div>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>