<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subc.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="twsub">Máquina de curvar tubos hidráulica semi automática – CTA</h1>
					<h2 class="lalign">MODELO CTA-1001</h2>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<img src="../../assets/img/maquinas/curvadora/cta.jpg" alt="Máquina de curvar tubos hidráulica – semi automática – CTA" class="img-responsive">
			</div>
			<div class="row">
				<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
				<p>
					Máquina hidráulica motorizada para curvar tubos com parede fina ou grossa de marca Farex, modelo CTA-1001, com as seguintes características:
					ESTRUTURA reforçada construída com chapas de aço laminado ASTM A-36, soldadas entre si e submetidas a alivio de tensões, com conjunto central dotado de pontos para fixação e alojamento na sua parte interna para o mecanismo hidráulico de giro, composto de atuador hidráulico e a unidade hidráulica de acionamento. <br>
					Sistema de acionamento manual e fechamento rápido (grampo torpedo), roletes robustos para guia do tubo, suporte da matriz e opcionalmente suporte para uso com tira rugas. <br>
					Angulo a ser curvado, de 0 a 100 graus;
					Com a utilização do mandril interno ou espiga, a deformação externa na zona da curva fica reduzida ao mínimo. <br>
					Painel de comando por meio de botoeira com cabo de 2,0 mts de comprimento. 	<br>
					Diâmetros a curvar até 50 x 3,0 mm.
				</p>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>