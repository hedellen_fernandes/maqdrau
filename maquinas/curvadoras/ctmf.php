<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subc.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="vcenter">Máquina de curvar tubos manual – CTFM 1 e CTFM 4</h1>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<img src="../../assets/img/maquinas/curvadora/ctfm1e4.jpg" alt="Máquina de Curvar Tubos Manual – CTFM 1 e CTFM 4" class="img-responsive">
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
					<p class="desc">
						Máquina de curvar tubos com acionamento manual para tubos, marca Farex, modelo CTFM-1, com coluna de perfil redondo, cabeçote superior, cabeçote inferior e base da coluna em Ferro Nodular, com 7 matrizes usinadas e 7 réguas fundidas, para tubos de Ø 3/8” a 1.1/4”com paredes de até 1,5 mm de espessura. <br>
						As máquinas modelo CTFM, por serem maquinas de acionamento manual, destinam-se a pequenas produções.
					</p>
				</div>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>