<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subc.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="twsub">Máquina de curvar tubos hidráulica - CTN 15</h1>
					<h2 class="lalign">ACIONAMENTO MANUAL / MOTORIZADO</h2>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-xs-12 col-sm-12">
					<img src="../../assets/img/maquinas/curvadora/ctn15.jpg" alt="Máquina de curvar tubos hidráulica – acionamento manual / motorizado – CTN 15" class="img-responsive">
				</div>
				<div class="col-md-6 col-xs-12 col-sm-12">
					<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
					<p class="desc">
						Máquina hidráulica manual de curvar tubos, marca Farex, modelo CTN-15, com jogo de 08 matrizes, de Ø ½” a 3” e 2 contra-estampos para tubos norma DIN 2440/2441 e Shedulle 40.
					</p>
				</div>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>