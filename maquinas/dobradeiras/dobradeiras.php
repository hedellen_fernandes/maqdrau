<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="../../maquinas.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="vcenter">Prensa Dobradeira Hidráulica de Chapa</h1>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<img src="../../assets/img/maquinas/dobradeiras/dobradeira.jpg" alt="Prensa Dobradeira Hidráulica de Chapa" class="img-responsive">
			</div>
			<div class="row">
				<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
				<p>
					Fabricadas no Brasil as Prensa Dobradeira Hidráulica de Chapa, marca Farex, possuem capacidades que variam de 70 até 800 toneladas e comprimentos de dobra de 2.100 á 6.100 mm, com as características construtivas, operacionais e dimensionais como seguem: <br>
					Opções de Comando CNC ou CLP.<br>
					Estrutura em chapa de aço ASTM-A36, soldadas e submetidas a alívio de tensões formando um conjunto rígido e estável.<br>
					Sistema de guias planas ajustáveis montadas entre os montantes e avental permitindo assim perpendicularidade perfeita.<br>
					Intermediários ajustáveis para fixação do punção (ferramenta superior).<br>
					Matriz inferior e Punção no modelo standard Farex, fornecidos em cortesia.<br>
					Máquina hidráulica com 02 velocidades proporcionando velocidade rápida de aproximação e reduzida em prensagem, com válvula de segurança contra sobrecargas.<br>
					Pedaleira, com botão de emergência central e rele de segurança interligado a maquina por tomada multipinos e cabo flexível.
				</p>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>