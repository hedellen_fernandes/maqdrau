<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="../../maquinas.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="vcenter">Guilhotina hidráulica corte vertical</h1>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<img src="../../assets/img/maquinas/guilhotina/guilhotina.jpg" alt="Guilhotina Hidráulica Corte Vertical" class="img-responsive">
			</div>
			<div class="row">
				<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
				<p>
					Guilhotina Hidráulica de corte vertical de marca FAREX, com capacidade para cortes de 1.500 até 6.100 mm e espessuras que variam de 6,40 mm até 30 mm em Aço Carbono (42 Kg/mm²) e Aço Inox (60 Kg/mm2.) até 20 mm de espessura. 
					Estruturas em chapas de Aço ASTM-A36, soldadas formando um conjunto em monobloco, submetidas a alivio de tensões. <br>
					Sistema de corte vertical com ângulo variável, com mesa móvel guiada no comprimento total por rolamentos extremamente robustos, assentados perfeitamente em pistas de aço tratadas termicamente e retificadas. <br>
					Prensa chapas de acionamento hidráulico e com ajuste automático de força. <br> 
					Pedestal de acionamento com pedaleira e com botão emergência incluso. <br>
					Painel de Comando com proteção IP-54, posicionado no avental fixo da máquina. <br>
					Com Controlador Lógico Programável, CLP com contador de golpes, e com IHM touchscreen. <br>
					Facas dotadas de quatro arestas de corte. Dispositivo limitador da descida da faca. <br>
					Ajuste rápido da folga das facas. Grade de proteção frontal, lateral na garganta e parte traseira. <br> 
					Iluminação para parte interna da maquina e área de corte.
				</p>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>
