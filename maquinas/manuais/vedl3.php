<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subvm.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="twsub">VIRADEIRAS MANUAIS 3 METROS</h1>
					<h2 class="lalign">VEDL 3</h2>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<img src="../../assets/img/maquinas/viradeira/vedl3.png" alt="VIRADEIRAS MANUAIS 3 METROS" class="img-responsive">
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
					<table class="table table-striped">
						<tr>
							<td>Comprímento útil de Dobra (mm)</td>
							<td>3.050</td>
						</tr>
						<tr>
							<td>Abertura(mm)</td>
							<td>56</td>
						</tr>
						<tr>
							<td>Peso Aproximado(Kg)</td>
							<td>900</td>
						</tr>
						<tr>
							<th colspan="2">ESPESSURA MÁXIMA DE DOBRA(mm)</th>
						</tr>
						<tr>
							<td>Aço Recozido</td>
							<td>1.20</td>
						</tr>
						<tr>
							<td>Aço 1010/1020</td>
							<td>0.90</td>
						</tr>
						<tr>
							<td>Aço Inox 304</td>
							<td>0.60</td>
						</tr>
						<tr>
							<td>Alumínio Mole</td>
							<td>1.40</td>
						</tr>
						<tr>
							<td>Cobre</td>
							<td>1.00</td>
						</tr>
						<tr>
							<td>Latão</td>
							<td>1.20</td>
						</tr>

					</table>
				</div>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>
