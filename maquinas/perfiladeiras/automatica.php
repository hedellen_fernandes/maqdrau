<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subcalhas.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="vcenter">PERFILADEIRA AUTOMÁTICA DE CALHAS</h1>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<img src="../../assets/img/maquinas/perfiladeira/automatica.png" alt="PERFILADEIRA AUTOMÁTICA DE CALHAS" class="img-responsive">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<p>
						A linha compacta de Perfiladeira Automática de Calhas Roboter tem como principal vantagem a facilidade de operação, sendo que com apenas alguns toques no painel de comando é possível fabricar 15 metros de calha residencial em apenas um minuto. Sem contar a alta produtividade que pode chegar a 5000 metros de calhas em apenas um turno de 8 horas trabalhadas por dia. São 100.000 metros de calhas por mês. <br>   

						Para a Perfiladeira de Calhas temos  perfil com desenvolvimento 250 - 300 - 350 - 400, o qual você pode definir na compra a largura e o perfil que a máquina irá produzir. <br>

						Projetada para fabricar um único perfil  (que você decide na compra com o desenvolvimento 250 - 300 - 350 - 400) que se adapta a todos os tipos de telhados, a perfiladeira de calhas compacta Roboter permite controlar o uso da quantidade de chapas através do menu status do sistema. Basta apenas programar a quantidade, comprimento das peças desejadas e apertar o botão Liga, o resto a perfiladeira de calhas Roboter se encarregará de fazer.
					</p>
					<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
					<p>
						Comprimento: 3200 mm. <br>
						Largura: 700 mm; Altura 1600 mm (sem bobina )<br>
						Motor principal 3HP Trifásico<br>
						Unidade hidráulica de 30 litros<br>
						Corte guilhotina automática hidráulica<br>
						Velocidade de trabalho 15 m/min.<br>
						Comando PLC- Computador integrado<br>
						Materiais alumínio 0,60mm a 0,80 mm/ Aço galvanizado ou Galvalume 0,50 mm.<br>
						Desbobinador com capacidade de 200kg<br><br>

						<a href="https://www.youtube.com/watch?v=LXkLit39xdg" class="btn bnt-alert videolink" title="VEJA O VIDEO" target="_blank"><span>VEJA O VÍDEO</span></a>
						
						<iframe width="560" height="315" src="https://www.youtube.com/embed/LXkLit39xdg" frameborder="0" allowfullscreen></iframe>
					</p>
				</div>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>
