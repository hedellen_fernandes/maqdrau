<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subdobradeiras.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="vcenter">DOBRADEIRA DE CURVAS DE CONDUTOR</h1>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<img src="../../assets/img/maquinas/perfiladeira/condutor.png" alt="DOBRADEIRA DE CURVAS DE CONDUTOR" class="img-responsive">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h1>ESPECIFICAÇÕES TÉCNICAS</h1>

					<p>
						Produz com qualidade e agilidade; <br>
						Equipamento compacto; <br>
						Utilize sobras de materiais para gerar lucro; <br>
						Troca rápida das ferramentas; <br>
						Alta produção; <br>
						Baixo custo. <br>
					</p>
				</div>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>
