<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subdobradeiras.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="vcenter">DOBRADEIRA DE SUPORTE ELETROCALHA</h1>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<img src="../../assets/img/maquinas/perfiladeira/suporte-calhas.png" alt="DOBRADEIRA DE SUPORTE ELETROCALHA" class="img-responsive">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<p>
						Nasce mais um produto da linha de dobradeiras de suporte, DOBRADEIRA DE SUPORTE DE ELETROCALHA, um equipamento adequado a NR12 (Norma de Segurança Brasileira). <br>

						É uma máquina moderna, com alta tecnologia e capacidade de produção adequada a aumentar os lucros da sua empresa.<br>

						Dobradeira de suporte de eletrocalha é acoplada a um desbobinador onde trabalha em modo contínuo, o equipamento fura e dobra o modelo do suporte conforme desenvolvido na sua memória.<br>

						Equipamento controlado por CNC, garante alta produção de confiabilidade de medidas.<br>

						Você pode ter vários modelos na memória, onde atreves de um clique a máquina já reduz custos de produção.  
						
					</p>
					
					<h1>ESPECIFICAÇÕES TÉCNICAS</h1>

					<p>
						Comprimento 4000 mm; <br>
						Largura 2000 mm; <br>
						Altura 1500 mm; <br>
						Peso 1200 Kg; <br>
						Velocidade de trabalho de 5 a 80m/min.; <br>
						Controlada por CNC; <br>
						Desbobinador duplo ou simples; <br>
						Capacidade de desbobinador 1200 Kg.; <br>
						Sistema de corte hidráulico; <br>
						Estação de estampo hidráulico; <br>
						Produto Nacional. <br> <br>
						<a href="https://www.youtube.com/0JLGBZwTjqk" class="btn bnt-alert videolink" title="VEJA O VIDEO" target="_blank"><span>VEJA O VÍDEO</span></a>
						<iframe width="560" height="315" src="https://www.youtube.com/embed/0JLGBZwTjqk" frameborder="0" allowfullscreen></iframe>
					</p>
				</div>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>
