<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subcalhas.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="vcenter">PERFILADEIRA DE CALHAS - MAX</h1>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<img src="../../assets/img/maquinas/perfiladeira/max.png" alt="PERFILADEIRA DE CALHAS MAX" class="img-responsive">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
					<p>
						Comprimento: 2700 mm; Largura: 700 mm; <br>
						Altura s/ base: 800 mm (sem bobina); <br>
						Peso: Aprox. 450 kg (sem bobina); <br>
						Fonte de alimentação elétrica: 220 volts monofásico – 8 ampères; <br> Motorização: Motor elétrico NEMA 2 CV monofásico com redutor de rosca sem fim acoplado; <br>
						Materiais: Alumínio = 0,6mm a 0,8mm / Aço galvanizado ou Galvalume = #26 (0,50mm); <br>
						Largura da bobina: 300 mm; <br>
						Capacidade do desbobinador padrão: 200 kg; <br>
						Perfis padrão Roboter: Tipo escadinha ou colonial, com e sem aba traseira; <br>
						Corte: Manual por guilhotina de alavanca; <br>
						Tração: Três estágios de rodas usinadas em poliuretano engrenadas através de correntes e engrenagens; <br>
						Velocidade: Até 15 metros por minuto; <br>
						<br>
						<iframe width="560" height="315" src="https://www.youtube.com/embed/mqO7dF2xvMQ" frameborder="0" allowfullscreen></iframe>
						<a href="https://www.youtube.com/mqO7dF2xvMQ" class="btn bnt-alert videolink" title="VEJA O VIDEO" target="_blank"><span>VEJA O VÍDEO</span></a>
						
					</p>
				</div>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>
