<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subcalhas.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="vcenter">PERFILADEIRA DE PINGADEIRA</h1>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<img src="../../assets/img/maquinas/perfiladeira/pingadeiras.png" alt="PERFILADEIRA DE PINGADEIRA" class="img-responsive">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
					<p>
						PRODUZA ATÉ 5 TAMANHOS DE PERFIL NA MESMA MÁQUINA. <br>
						Leve e compacta ideal para levar até a obra. <br>
						Alta produção; <br>
						Produto personalizado com reforço; <br>
						Peças sem emendas. <br>
						<br>
						<a href="https://www.youtube.com/6vRbXn6lews" class="btn bnt-alert videolink" title="VEJA O VIDEO" target="_blank"><span>VEJA O VÍDEO</span></a>
						<iframe width="560" height="315" src="https://www.youtube.com/embed/6vRbXn6lews" frameborder="0" allowfullscreen></iframe>
					</p>
				</div>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>
