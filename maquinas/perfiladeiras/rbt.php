<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subdobradeiras.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="vcenter">DOBRADEIRA SUPORTE MULTI MODELOS RBT DSH2</h1>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<img src="../../assets/img/maquinas/perfiladeira/dsh2.png" alt="DOBRADEIRA SUPORTE MULTI MODELOS RBT DSH2" class="img-responsive">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<p>
						Esta é a 2º geração da Dobradeira de Suporte Multi Modelos produzido pela Roboter. Projetada para a confecção de suportes de calhas em barras chatas de alumínio ou de ferro, com sistema de furação por estampo. <br>

						Conta com memória super prática para até 30 modelos. <br>

						Este equipamento tem alta produtividade, contando com tecnologia de ponta.
						
					</p>
					
					<h1>ESPECIFICAÇÕES TÉCNICAS</h1>

					<p>
						2º geração de dobradeira de suporte <br>
						Equipamento hidráulico <br>
						Sistema de dobras com servo motor <br>
						Furação programada ao longo do suporte <br>
						30 memórias que permitem alterações <br> <br>
						<iframe width="560" height="315" src="https://www.youtube.com/embed/ZRHFrIg1IME" frameborder="0" allowfullscreen></iframe>
						<a href="https://www.youtube.com/ZRHFrIg1IME" class="btn bnt-alert videolink" title="VEJA O VIDEO" target="_blank"><span>VEJA O VÍDEO</span></a>

					</p>
				</div>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>
