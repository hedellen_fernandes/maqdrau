<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subcalhas.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="vcenter">PERFILADEIRA SEMI AUTOMÁTICA DE CALHAS</h1>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<img src="../../assets/img/maquinas/perfiladeira/semi-automatica.png" alt="PERFILADEIRA SEMI AUTOMÁTICA DE CALHAS" class="img-responsive">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<p>
						A linha compacta de Perfiladeiras Semi-Automáticas Roboter tem como principal vantagem a facilidade de operação, sendo que com apenas alguns toques no painel de comando é possível fabricar quinze metros de calha residencial em apenas um minuto. <br>

						Projetada para fabricar um único perfil (que você esolhe na compra com o desenvolvimento 250 - 300 - 330- 350 ou 400) que se adapta a todos os tipos de telhados, a perfiladeira de calhas compacta Roboter permite controlar o uso da quantidade de chapas através do menu status do sistema. Basta apenas programar o comprimento de cada peça e apertar o botão Liga, a perfiladeira irá produzir a calha e o corte por meio de guilhotina manual onde o operador acionará a alavanca para o corte da calha. Para fabricar mais peças somente ligar o equipamento novamente na ultima medida programada. <br> 

						Simples e muito eficiente. <br>
						
					</p>
					
					<h1>ESPECIFICAÇÕES TÉCNICAS</h1>

					<p>

						Comprimento: 3200 mm; <br>
						Largura: 700 mm; <br> Altura 1600 mm (sem bobina); <br>
						Motor principal 3HP Trifásico; <br>
						Corte guilhotina manual/
						Velocidade de trabalho 15 m/min; <br>
						Comando PLC- Computador integrado para medir comprimentos; <br>
						Materiais alumínio 0,60mm a 0,80 mm/ Aço galvanizado ou Galvalume 0,50 mm; <br>
						Desbobinador com capacidade de 200kg. 
					</p>
				</div>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>
