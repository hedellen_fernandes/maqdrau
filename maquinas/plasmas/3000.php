<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subpl.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="vcenter ccenter">Plasma CNC PL3000 - Galli</h1>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<img src="../../assets/img/maquinas/plasmas/3000.jpg" alt="Plasma CNC PL3000 - Galli" class="img-responsive">
			</div>
			<div class="row">
				<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
				<p>
					Linha leve: PL3000 CNC - Galli <br>
					Acompanha  mesa de apoio de chapas (cuba d’agua) <br>
					Acompanha software de aproveitamento <br>
					Sensor de controle de altura da (tocha capacitiva) <br>
					Duplamente motorizada por servo motor Panasonic <br>
					Guias lineares e patins. <br>
					Pinhão e cremalheira modulo 1 ½ <br>
					Comando CNC nacional (Galli) <br>
					Preparada para receber somente fonte plasma
				</p>
				<p>Fabricamos, vendemos máquinas de Corte Plasma e deixaremos em pleno funcionamento em sua empresa, o seu contato poderá ser feito diretamento conosco, iremos avaliar o seu pedido e sua necessidade por um de nossos consultores, assim identificaremos o melhor projeto custo benefício. Agora temos planos para manutenção preventiva pois mantemos uma equipe de técnicos altamente qualificados à oferecer excelente produto e uma atenção após a venda.</p>
				
			</div>
		</div>
	</section>
<?php include '../footer.php';?>