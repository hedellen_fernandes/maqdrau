<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subpl.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="vcenter ccenter">Plasma CNC PL3030</h1>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<img src="../../assets/img/maquinas/plasmas/3030.jpg" alt="Plasma CNC PL3030" class="img-responsive">
			</div>
			<div class="row">
				<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
				<p>
					Linha pesada já acoplada com oxicorte  e preparada para receber fonte plasma de alta Definição  de corte. <br>
					Acompanha uma caneta de oxicorte para corte ate 100 mm, mesa de apoio (cuba d’agua)  software  de aproveitamento  nest
					Comando CNC  dedicado Sirius touch screen de 7’’ ou de 12’’ 
					THC  eletrônico por tenção de arco.
				</p>
				<p>Fabricamos, vendemos máquinas de Corte Plasma e deixaremos em pleno funcionamento em sua empresa, o seu contato poderá ser feito diretamento conosco, iremos avaliar o seu pedido e sua necessidade por um de nossos consultores, assim identificaremos o melhor projeto custo benefício. Agora temos planos para manutenção preventiva pois mantemos uma equipe de técnicos altamente qualificados à oferecer excelente produto e uma atenção após a venda.</p>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>