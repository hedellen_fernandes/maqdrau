<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subs.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="vcenter">DESBOBINADOR EXPANSIVO</h1>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<img src="../../assets/img/maquinas/slitter/des.png" alt="SLITTER DSR100" class="img-responsive">
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
					<table class="table table-striped">
						<tr></tr>
						<tr>
							<td colspan="2" class="sub-dsr no-padding-top">DESBOBINADOR EXPANSIVO <br> <p>445/530 Ø - 545/630 Ø</p></td>
						</tr>
						<tr>
							<td>Capacidade (t)</td>
							<td>15</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>
