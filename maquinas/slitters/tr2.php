<?php include"../header.php";?>
	<section id="topo" class="contato maquinas">
		<div class="container">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-sx-1">
					<a href="subs.php" class="bnt btn-danger"></a>
				</div>
				<div class="col-md-11 col-sm-11 col-sx-11">
					<h1 class="vcenter">SLITTER TR2</h1>
				</div>
			</div>
		</div>
	</section>
	<section id="categoria">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<img src="../../assets/img/maquinas/slitter/tr2.png" alt="SLITTER TR2" class="img-responsive">
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<h1>ESPECIFICAÇÕES TÉCNICAS</h1>
					<table class="table table-striped">
						<tr>
							<td>Capacidade de Corte (mm)</td>
							<td>1.00</td>
						</tr>
						<tr>
							<td>Largura de Corte (mm)</td>
							<td>1.300</td>
						</tr>
						<tr>
							<td>Qualidade Disco Móvel (pç)</td>
							<td>4</td>
						</tr>
						<tr>
							<td>Peso Aproximado(Kg)</td>
							<td>450</td>
						</tr>
						<tr>
							<td>Potência do Motor(cv)</td>
							<td>1</td>
						</tr>
						<tr>
							<td>Velocidade de Corte (m/min)</td>
							<td>21</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="row">
				<a href="https://www.youtube.com/watch?v=0kkVqY4fDQQ" class="btn bnt-alert videolink" title="VEJA O VIDEO" target="_blank"><span>VEJA O VÍDEO</span></a>
				<iframe width="420" height="315" src="https://www.youtube.com/embed/0kkVqY4fDQQ" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
	</section>
<?php include '../footer.php';?>
