<?php include "header.php";?>
	<section id="corpoServico" class="treinamento">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h1 class="sub">TREINAMENTO OPERACIONAL</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<img class="img-responsive" src="assets/img/servicos/treinamento.jpg" alt="Carro Assistência Técnica Maqdrau">
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<p>Oferecemos aos nossos clientes treinamento operacional completo para toda nossa linha, incluindo máquinas, equipamentos e ferramentas. O treinamento é ministrado in loco, ou seja, o operador será treinado na máquina em que irá trabalhar, sendo orientado para uso e características de todos os recursos oferecidos pelo equipamento bem como orientado para a correta utilização da máquina e aplicação de seus recursos aos produtos para os quais o equipamento será destinado.
					</p>
					<p>Como parte do nosso programa de pós venda, também oferecemos treinamento operacional a título de reciclagem ou em casos de substituição de operadores.</p>
				</div>
			</div>
		</div>
	</section>
<?php include "footer.php";?>
